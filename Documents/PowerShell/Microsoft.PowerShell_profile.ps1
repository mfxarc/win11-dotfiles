# Custom prompt
function prompt { "$($executionContext.SessionState.Path.CurrentLocation) ~ " }
# Aliases
function qq { clear }
function ee { ls }
function dw { cd "$HOME\Downloads" }
function dc { cd "$HOME\Documents" }
function yt { yt-dlp -o '%(title)s.%(ext)s' @Args }
function systime { (get-date) - (gcim Win32_OperatingSystem).LastBootUpTime }
function dots { git --git-dir="$HOME\.dotfiles" --work-tree="$HOME" @Args }